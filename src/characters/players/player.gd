extends KinematicBody2D

export var SPEED = 10000

var velocity = Vector2.ZERO

func _physics_process(delta: float) -> void:
	var direction = Vector2(
		Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left"),
		Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")
	)
	velocity = direction.normalized() * SPEED
	move_and_slide(velocity)
